#include "Player.h"
#include "StripLedScreen.h"

Player::Player(float nx, float ny) {
  x = nx;
  y = ny;
  startX = nx;
  startY = ny;
}


void Player::begin() {
  // do nothing
}

void Player::kill() {
  alive = false;
}


void Player::reset() {
  alive = true;
  score = 0;
  x = startX;
  y = startY;
  vspeed = 0;
  hspeed = 0;
}
bool Player::jump() {
  if (!alive) return false;
  if (y==floor) {
    vspeed = -0.2;
    return true;
  }
  return false;
}

void Player::loop() {
  
  x+= hspeed;
  y+= vspeed;
  if (y<0) y=0;
  if (y>floor) { // si esta arriba del piso
    y = floor;
    vspeed = 0;
  } else {
    if (y!=floor) {
      vspeed+=gravity;
    }
  }

  frame+=anim_speed;
}

void Player::draw(StripLedScreen *screen) {
 if (!alive) {
   screen->setPixel( x+2, y, screen->verde);
   screen->setPixel( x+1, y, screen->verde);
   screen->setPixel( x, y, screen->verde);
   screen->setPixel( x+2, y-1, screen->rojo);
   screen->setPixel( x+3, y, screen->verde);
  return;
 }
 if (vspeed==0) { // PARADO
   screen->setPixel( x+1, y-1, screen->verde);
   screen->setPixel( x+1, y-2, screen->verde);
   screen->setPixel( x+1, y-3, screen->rojo);
   screen->setPixel( x+2, y-2, screen->verde);
   screen->setPixel( x, y-2, screen->verde);
   if (frame<1) {
    screen->setPixel( x+1, y, screen->verde);
   } else {
    screen->setPixel( x, y, screen->verde);
    screen->setPixel( x+2, y, screen->verde);
    if (frame>2) frame=0;
   }
 } else { // SALTO
   screen->setPixel( x+1, y, screen->verde);
   screen->setPixel( x+1, y-1, screen->verde);
   screen->setPixel( x+1, y-2, screen->rojo);
   screen->setPixel( x+2, y-2, screen->verde);
   screen->setPixel( x, y-1, screen->verde);
   screen->setPixel( x, y, screen->verde);
   screen->setPixel( x+2, y-1, screen->verde);
 }
}
