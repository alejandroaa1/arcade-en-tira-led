#include "Sprite.h"  

Sprite::Sprite(byte width, byte height, byte * image) {
  _height = height;
  _width = width; 
  pixels = image;
};

void Sprite::setColor(uint32_t ccolor) {
  color = ccolor;
}

int Sprite::getHeight() {
  return _height;
};

int Sprite::getWidth() {
  return _width;
};
