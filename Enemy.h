#ifndef ENEMY_H
#define ENEMY_H
#include "StripLedScreen.h"
#include "Player.h"

class Enemy
{
  public:
    Enemy();
    int offsetx = 0;
    int offsety = -1;
    int width = 2;
    int height = 2;
    bool alive = true;
    void begin();
    void loop(Player *player);
    void jump();
    void spawn(int score);
    bool collisionPlayer(Player *player);
    void draw(StripLedScreen* screen);
    float x;
    float y;
    float hspeed=0;
    float vspeed=0;
    float frame = 0;
    float gravity = 0.05;
    int floor = 7;
    float anim_speed = 0.05;
    void generateShape(int s);
    void kill();
  private:
    int shape=0;
};
#endif
