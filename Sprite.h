#ifndef SPRITE_H
#define SPRITE_H
#include "Arduino.h"

class Sprite 
{
  public:
    Sprite (byte width, byte height, byte * image);
    byte * pixels;
    uint32_t color;
    int getHeight();
    int getWidth();
    void setColor(uint32_t ccolor);
  private:
    byte _width;
    byte _height;
};

#endif
