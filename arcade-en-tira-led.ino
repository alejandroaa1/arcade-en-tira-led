#include <Adafruit_NeoPixel.h>
#include "StripLedScreen.h"
#include "Player.h"
#include "Enemy.h"

// PINOUT
#define PIN_AUDIO 3
#define PIN_ACTION 4
#define PIN_LED_SCREEN 8

#define ENEMY_COUNT 3
// Para la maquina de estados finita: solo dos estados por ahora
#define WAIT_SCREEN 0
#define GAME 1

// objetos
StripLedScreen screen(8,32,PIN_LED_SCREEN); // creacion de la pantalla
Player player(9,7); // creacion del objeto jugador
Enemy enemies[ENEMY_COUNT]; // creacion de los objetos enemigos, por el momento no se usa
// estado del juego
int state = WAIT_SCREEN;

// mayor puntaje
int HISCORE = 0; // solo en ram, tras reinicio se pierdel
unsigned long int time = 0; // un timer

byte m[] = {0b1111,0b1011,0b1111,0b1011,0b1111};
Sprite img = Sprite(4,5,m);

void setup() {
  
  // put your setup code here, to run once:
  Serial.begin(9600); // para debug
  screen.begin(); // inicializacion de la pantalla
  
  pinMode(PIN_ACTION,INPUT);
  pinMode(PIN_AUDIO,OUTPUT);
  
  for (int i=0;i<ENEMY_COUNT;i++) { // instanciacion de los enemigos
    enemies[i] = Enemy();
  }
}

void spawnAnEnemy() { // instanciar un enemigo
  for (int i=0;i<ENEMY_COUNT;i++) {
    if (!enemies[i].alive) {
      enemies[i].spawn(player.score); // la complejidad depende del puntaje
      return;
    }
  } 
}

int enemiesAlive() { // contador de enemigos activos
  int count = 0;
  for (int i=0;i<ENEMY_COUNT;i++) {
    if (enemies[i].alive) {
      count+=1;
    }
  } 
  return count;
}

void gameLoop() { // loop del juego principal
    screen.clean();
    
    //gestion de entradas
    if (digitalRead(PIN_ACTION)==HIGH) {
      if (player.jump()) {
        tone(PIN_AUDIO,1100,10);
      }
    }

    // gestion de enemigos
    for (int i=0;i<ENEMY_COUNT;i++) {
      enemies[i].loop(&player); // simulacion de loop
      enemies[i].collisionPlayer(&player); // verficiacion de colisiones
    } 

    // gestion del jugador
    player.loop();
    player.draw(&screen);
  
    // dibujado en pantalla
    for (int i=0;i<ENEMY_COUNT;i++) {
      enemies[i].draw(&screen);
    }
    screen.drawDigits(0, 1,player.score, screen.azul,2);  

    // gestion de obstaculos
    if (enemiesAlive()==0) {
      spawnAnEnemy();
    }
    if (!player.alive) {
      lose();
    }

    // actualizacion de los leds en la pantalla
    screen.render();
}

void drawArrow(int xM, int yM) { // dibuja una flecha 
  for (int i=0; i<7; i++) {
    screen.setPixel(xM+i,yM,screen.verde);
  }
  screen.setPixel(xM+4,yM-1,screen.verde);
  screen.setPixel(xM+4,yM-2,screen.verde);
  screen.setPixel(xM+4,yM+1,screen.verde);
  screen.setPixel(xM+4,yM+2,screen.verde);
  
  screen.setPixel(xM+5,yM-1,screen.verde);
  screen.setPixel(xM+5,yM+1,screen.verde); 
}


void waitLoop() { // en el estado de espera se ejecuta este loop
  time+=1; // un timer para animaciones
  
  screen.clean(); // limpieza de la pantalla

  //img.setColor(screen.rojo);
  //screen.drawSprite(0,0,&img);
  screen.drawText(0,0,screen.azul, "Hola");
  if (time<100) { // dibujo "animado" de la flecha"
    drawArrow(10,4);
  } else {
    drawArrow(9,4);
    if (time>200) {
      time = 0;
    }
  };
  // dibujo de hiscore
  screen.drawDigits(20, 2,HISCORE, screen.azul,2); 

  // verfica inicio de juego
  if (digitalRead(PIN_ACTION)==HIGH) {
    state = GAME;
    
    tone(PIN_AUDIO,400);
    screen.clean();
    screen.drawDigits(14, 1,3, screen.verde,1);
    screen.render();
    delay(500);
    
    screen.clean();
    screen.render();
    noTone(PIN_AUDIO);
    delay(500);
    
    tone(PIN_AUDIO,400);
    screen.clean();
    screen.drawDigits(14, 1,2, screen.verde,1);
    screen.render();
    delay(500);
       
    screen.clean();
    screen.render();
    noTone(PIN_AUDIO);
    delay(500);
    
    tone(PIN_AUDIO,600);
    screen.clean();
    screen.drawDigits(14, 1,1, screen.verde,1);
    screen.render();
    delay(500);
    noTone(PIN_AUDIO);
  }

  // actualizacion de pantalla
  screen.render();
}

void loop() {
  //Borrado de la pantalla
  
  switch (state == GAME) { // maneja los estados del juego
    case GAME:
      gameLoop(); // si es el juego 
    break;
    case WAIT_SCREEN:
      waitLoop(); // si es la pantalla de inicio
    break;
  }
}

void lose() { // accion de perder
    // musica perdio
    tone(PIN_AUDIO,800);
    delay(500);
    tone(PIN_AUDIO,440);
    delay(500);
    tone(PIN_AUDIO,200);
    delay(800);
    noTone(PIN_AUDIO);
    screen.clean();
    screen.render();
    delay(1000);
    // gestion de puntaje
    if (player.score > HISCORE) {
      HISCORE = player.score;
      // MUSIC HISCORE
      tone(PIN_AUDIO,700);
      delay(200);
      tone(PIN_AUDIO,800);
      delay(200);
      tone(PIN_AUDIO,900);
      delay(300);
      tone(PIN_AUDIO,900);
      delay(300);
      noTone(PIN_AUDIO);
      screen.clean();
      screen.render();
      delay(500);
      for (int i=0; i<5; i++) {
        screen.clean();
        screen.drawDigits(14, 2,player.score, screen.azul,2);  
        screen.render();
        delay(500);
        screen.clean();
        screen.render();
        delay(500);        
      }
      delay(1000);
      
    };
    stopGame(); 
}

void startGame() { // accion de iniciar el juego
  state = GAME;
}

void stopGame() { // detener el juego
  player.reset();
  for (int i =0; i< ENEMY_COUNT;i++) {
    enemies[i].kill();
  }
  state = WAIT_SCREEN;
  delay(100);
}
