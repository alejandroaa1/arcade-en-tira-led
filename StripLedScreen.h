#ifndef STRIP_LED_SCREEN_H
#define STRIP_LED_SCREEN_H

#include <Adafruit_NeoPixel.h>
#include <avr/power.h>
#include "Arduino.h"
#include "Sprite.h"


class StripLedScreen
{
  public:
    StripLedScreen(int w, int h, int pin);
    uint32_t rojo = pixels.Color(10,0,0);
    uint32_t verde = pixels.Color(0,10,0);
    uint32_t azul = pixels.Color(0,0,10);
    Adafruit_NeoPixel pixels;

    void begin();
    void drawSingleNumber(int x, int y,int number, uint32_t color);
    void drawDigits(int x, int y,int number, uint32_t color,int count);
    void setPixel(int x, int y, uint32_t color);
    void drawSprite(int x, int y, Sprite * sprite);
    void clean();
    void render();
    int getHeight();
    int getWidth();
    void drawByteArray(int x, int y, byte * img, byte w, byte h, uint32_t color );
    void drawText(int x,int y,uint32_t color, String str);
  private:
    int _height;
    int _width; 
    int _pin;
    
    byte zero[5] = { 0b010,0b101,0b101,0b101,0b010};
    byte one[5] = { 0b010,0b110,0b010,0b010,0b111};
    byte two[5] = { 0b110,0b001,0b010,0b100,0b111};
    byte trhee[5] = { 0b110,0b001,0b010,0b001,0b110};
    byte four[5] = { 0b101,0b101,0b111,0b001,0b001};
    byte five[5] = { 0b111,0b100,0b110,0b001,0b110};
    byte six[5] = { 0b011, 0b100,0b110,0b101,0b111};
    byte seven[5] = { 0b111, 0b001,0b001,0b001,0b001};
    byte eight[5] = { 0b111,0b101,0b010,0b101,0b111};
    byte nine[5] = { 0b111,0b101,0b111,0b001,0b001};    
  

    byte a[5] = {0b111,0b101,0b111,0b101,0b101}; 
    byte b[5] = {0b110,0b101,0b111,0b101,0b111}; 
    byte c[5] = {0b111,0b100,0b100,0b100,0b111}; 
    byte d[5] = {0b100,0b110,0b101,0b110,0b100};
    byte e[5] = {0b111,0b100,0b111,0b100,0b111};  
    byte f[5] = {0b111,0b100,0b111,0b100,0b100};  
    byte g[5] = {0b111,0b100,0b111,0b101,0b111};  
    byte h[5] = {0b101,0b101,0b111,0b101,0b101};  
    byte i[5] = {0b010,0b000,0b010,0b010,0b010};  
    byte j[5] = {0b111,0b001,0b001,0b101,0b111};
    byte k[5] = {0b101,0b101,0b110,0b101,0b101};  
    byte l[5] = {0b100,0b100,0b100,0b100,0b111};    
    byte m[5] = {0b101,0b101,0b111,0b101,0b101};  
    byte n[5] = {0b101,0b101,0b111,0b111,0b101};  
    byte o[5] = {0b111,0b101,0b101,0b101,0b111};  
    byte p[5] = {0b111,0b101,0b111,0b100,0b100};  
    byte q[5] = {0b111,0b101,0b111,0b001,0b111};  
    byte r[5] = {0b111,0b101,0b111,0b110,0b101};  
    byte s[5] = {0b111,0b100,0b110,0b001,0b111};  
    byte t[5] = {0b111,0b010,0b010,0b010,0b010};  
    byte u[5] = {0b101,0b101,0b101,0b101,0b111};  
    byte v[5] = {0b101,0b101,0b101,0b010,0b010};  
    byte w[5] = {0b101,0b101,0b101,0b111,0b101};
    byte x[5] = {0b101,0b010,0b010,0b010,0b101};
    byte y[5] = {0b101,0b101,0b010,0b010,0b110};
    byte z[5] = {0b111,0b001,0b111,0b100,0b111};

    byte *Alphabet[26] = {a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z};
};

#endif
