#include "Enemy.h"
#include "StripLedScreen.h"

Enemy::Enemy() {
  alive = false;
  x = 32;
  y = 6;  
}


void Enemy::begin() {
  // do nothing
}

void Enemy::jump() {
  if (y==floor) {
    vspeed = -0.8;
  }
}


void Enemy::spawn(int score) {
  alive = true;
  hspeed = -0.15 - score * 0.005;
  x = 32;
  int maxshape = random(score/3+1);
  if (maxshape > 4) maxshape = 4;
  generateShape( random(maxshape));
}

void Enemy::loop(Player *player) {
  if (!alive) return;
  x+= hspeed;
  y+= vspeed;
  if (y<0) y=0;

  if (y>floor) { // si esta arriba del piso
    y = floor;
    vspeed = 0;
  } else {
    if (y!=floor) {
      vspeed+=gravity;
    }
  }
  frame+=anim_speed;
  if (x<0) {
    if (player->alive) player->score+=1;
    alive = false;
  }
}

void Enemy::kill() {
  alive = false;
}

bool Enemy::collisionPlayer(Player *player) {
  if (!player->alive) return false;
  if (!alive) return false;
  
  if ((x+offsetx < player->x + player->width + player->offsetx) and (x + width +offsetx > player->x + player->offsetx) and  (y+offsety < player->y + player->height + player->offsety) and (height + y + offsety> player->y + + player->offsety)) {
      player->kill();
      Serial.print("Shape: "); Serial.println(shape);
      Serial.print("offsetY: "); Serial.println(offsety); 
      Serial.print("height: "); Serial.println(height); 
      return true;
  } 
  return false;
}

void Enemy::generateShape(int s) {
  shape = s;
  offsety = -1*shape;
  height = shape + 1;
}

void Enemy::draw(StripLedScreen *screen) {
 if (!alive) return;
  for (int i=0;i< shape + 1;i++) {
      screen->setPixel(x-1,y-i,screen->rojo);
      screen->setPixel(x+1,y-i,screen->rojo);
      screen->setPixel(x,y-i,screen->rojo);   
  }
  frame = 0;
}
