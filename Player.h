#ifndef PLAYER_H
#define PLAYER_H

#include "StripLedScreen.h"


class Player
{
  public:
    Player(float x, float y);
    int offsetx = 0;
    int offsety = -2;
    int width = 2;
    int height = 3;
    int score = 0;
    bool alive = true;
    void begin();
    void loop();
    void reset();
    bool jump();
    void kill();
    void draw(StripLedScreen* screen);
    float x;
    float y;
    float hspeed=0;
    float vspeed=0;
    float frame = 0;
    float gravity = 0.005;
    int floor = 7;
    float anim_speed = 0.05;
 private:
    int startX;
    int startY;
};

#endif
