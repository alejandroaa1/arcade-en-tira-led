#include <Adafruit_NeoPixel.h>
#include <avr/power.h>
#include "Arduino.h"
#include "StripLedScreen.h"

StripLedScreen::StripLedScreen(int w, int h, int pin) {
  _width = w;
  _height = h;
  _pin = pin;
}


void StripLedScreen::begin() {
  pixels = Adafruit_NeoPixel(_width * _height, _pin, NEO_GRB + NEO_KHZ800);
  pixels.begin();
}


void StripLedScreen::drawDigits(int x, int y, int number, uint32_t color, int count) {
  int aux = number;
  int n;
  for (int i = count - 1; i >= 0; i--) {
    n = aux / pow(10, i);
    drawSingleNumber(x + (count - 1 - i) * 4, y, n, color);
    aux = aux - n * pow(10, i);
  }
}

void StripLedScreen::drawSingleNumber(int x, int y, int number, uint32_t color) {
  byte *n;

  switch (number) {
    case 0: n = zero; break;
    case 1: n = one; break;
    case 2: n = two; break;
    case 3: n = trhee; break;
    case 4: n = four; break;
    case 5: n = five; break;
    case 6: n = six; break;
    case 7: n = seven; break;
    case 8: n = eight; break;
    case 9: n = nine; break;
  }
  for (int i = 0; i < 5; i++) {
    //
    if ((n[i] & 0b100) != 0) setPixel(x, y + i, color);
    if ((n[i] & 0b010) != 0) setPixel(x + 1, y + i, color);
    if ((n[i] & 0b001) != 0) setPixel(x + 2, y + i, color);
  }
}

void StripLedScreen::setPixel(int x, int y, uint32_t color) {
  if (x < 0) return;
  if (y < 0) return;
  if (y > 7) return;
  if (x > 31) return;

  int i = 255 - (x * _width + (1 - x % 2) * (7 - y) + (x % 2) * (y));
  pixels.setPixelColor(i, color);
}

int StripLedScreen::getHeight() {
  return _height;
}

int StripLedScreen::getWidth() {
  return _width;
}

void StripLedScreen::clean() {
  for (int i = 0; i < _width * _height ; i++) {
    pixels.setPixelColor(i, 0);
  }
}

void StripLedScreen::render() {
  pixels.show();
}

void StripLedScreen::drawText(int x,int y,uint32_t color, String str) {
  for (int i= 0; i < str.length(); i++) {
    drawByteArray(x+i*4,y, Alphabet[toupper(str[i])-41],3,5,color); // debuggear
    
  }
}


void StripLedScreen::drawByteArray(int x, int y, byte * img, byte w, byte h, uint32_t color ) {
  byte mask = 0b00000001;
  for (int i=0; i< h; i++) {
    if (y+i< 0) break;
    if (y+i > getHeight()) break;
    for (int j=0; j < w; j++) {
      if (x+j< 0) break;
      if (x+j > getWidth()) break;
      mask = 0b00000001;
      mask = mask << j;
      if ((img[i] & mask) != 0) {
        setPixel(x+w-j,y+i,color);
      };
    };
  };
}


void StripLedScreen::drawSprite(int x, int y, Sprite * sprite) {
  byte mask = 0b00000001;
  for (int i=0; i< sprite->getHeight(); i++) {
    if (y+i< 0) break;
    if (y+i > getHeight()) break;
    for (int j=0; j < sprite->getWidth(); j++) {
      if (x+j< 0) break;
      if (x+j > getWidth()) break;
      mask = 0b00000001;
      mask = mask << j;
      if ((sprite->pixels[i] & mask) != 0) {
        setPixel(x+sprite->getWidth()-j,y+i,sprite->color);
      };
    };
  };
};
